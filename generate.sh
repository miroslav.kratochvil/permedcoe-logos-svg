#!/bin/sh

# this generates all logo variants

#black&white variants (no color management at all)
BLACK=000000
WHITE=ffffff
INK=$BLACK ./full-1ink.sh > full-1ink-black.svg
INK=$WHITE ./full-1ink.sh > full-1ink-white.svg
FINGERPRINT=$BLACK PERMEDCOE=$BLACK ./plain.sh > plain-1ink-black.svg
FINGERPRINT=$WHITE PERMEDCOE=$WHITE ./plain.sh > plain-1ink-white.svg

#generic colored output
gen_colored() {
	INK=$BLUE ./full-1ink.sh > full-1ink-blue$suf
	FINGERPRINT=$BLUE PERMEDCOE=$BLUE ./plain.sh > plain-1ink-blue$suf

	FINGERPRINT=$PINK_FP_ONWHITE PERMEDCOE=$BLUE ./plain.sh > plain-light$suf
	FINGERPRINT=$PINK_FP PERMEDCOE=$WHITE ./plain.sh > plain-dark$suf
	FRAME_LEFT=$BLUE FINGERPRINT=$PINK_FP PERMEDCOE=$WHITE ./boxed.sh > boxed-blue$suf
	FRAME_LEFT=$WHITE FINGERPRINT=$PINK_FP_ONWHITE PERMEDCOE=$BLUE ./boxed.sh > boxed-white$suf

	FRAME_LEFT=$BLUE FINGERPRINT=$PINK_FP PERMEDCOE=$WHITE FRAME_RIGHT=$PINK_FRAME TEXT_RIGHT=$BLUE \
	./full-transparent.sh > full-light$suf
	FRAME_LEFT=$BLUE FINGERPRINT=$PINK_FP PERMEDCOE=$WHITE FRAME_RIGHT=$PINK_FRAME TEXT_RIGHT=$WHITE \
	./full-transparent.sh > full-dark$suf
	FRAME_LEFT=$BLUE FINGERPRINT=$PINK_FP PERMEDCOE=$WHITE FRAME_RIGHT=$PINK_FRAME_ONPINK TEXT_RIGHT=$BLUE \
	./full-transparent.sh > full-onpink$suf
	FRAME_LEFT=$BLUE_FRAME FINGERPRINT=$PINK_FP PERMEDCOE=$WHITE FRAME_RIGHT=$PINK_FRAME TEXT_RIGHT=$WHITE \
	./framed.sh > full-onblue$suf
	FRAME_LEFT=$BLUE FINGERPRINT=$PINK_FP PERMEDCOE=$WHITE FRAME_RIGHT=$PINK_FRAME TEXT_RIGHT=$BLUE \
	BACKGROUND_RIGHT=$WHITE ./full-background.sh > full-light-filled$suf
}

# screen colors
BLUE=001982
BLUE_FRAME=9c91cd
#PINK=dcb0b5 #not used
PINK_FRAME=e5c3c6
PINK_FRAME_ONPINK=f2e0e2
PINK_FP=e0b9bd
PINK_FP_ONWHITE=d29da3
suf=-screen.svg gen_colored

#print colors
BLUE=2d307e
BLUE_FRAME=9988cc
#PINK=dfa2b3
PINK_FRAME=e7b7c4
PINK_FRAME_ONPINK=f2d8df
PINK_FP=e2acbb
PINK_FP_ONWHITE=d790a5
suf=-print.svg gen_colored
